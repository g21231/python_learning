
# # 4. task

def get_int_arr(str):
    int_arr = []
    num_str = ""
    for i in range(len(str)):
        if str[i].isnumeric():
            num_str += str[i]
            if i + 1 == len(str) or not str[i + 1].isnumeric():
                int_arr.append(num_str)
                num_str = ""
    return int_arr
print(get_int_arr("data 48 call 9 read13 blank0"))
#
# #1. task
#
str = "         Hi      Davit,       Rafo,      Nerses          "
print(" ".join(str.split()))
#
#
# # 2. task
#
def removeDuplicate(word):
    if(len(word)==1):
        return word
    if word[0] == word[1]:
        return removeDuplicate(word[1:])
    return word[0] + removeDuplicate(word[1:])
word = "aaaaabbbbccccc"
print(removeDuplicate(word))
#
#
#
# # 8. task
def replace(text1, text2, replaceWith):
    return text1.replace(text2, replaceWith)
if __name__ == "__main__":
    text1 = input("Enter string: ")
    text2 = input("Enter what you want to change: ")
    replaceWith = input("Enter what to replace: ")
    print(replace(text1, text2, replaceWith))


# program to compute mod of a big number
# represented as string

# Function to compute num (mod a)

#1.Fill the array with random positive and negative numbers in such a way that all
# numbers are modulo different. This means that an array cannot contain only two equal numbers,
# but there cannot be two equal in absolute value. Find the largest modulo number in the resulting array.

def max_num(list):
    max = list[0]
    for a in list:
        if abs(a) > max:
            max = abs(a)
    return max

print(max_num([1, 2, -8, 0]))

#2.Find the index of the modulo minimum element of the array.
# For example, in the array [10, -3, -5, 2, 5], the modulo element is the number 2.

def min_num(list):
    min = list[0]
    for a in list:
        if abs(a) < min:
            min = abs(a)
    return min

print(min_num([10, -3, -5, 2, 5]))


#3.Calculate the sum of the absolute values of the array elements located after the first negative element.
#For example, in the array [5, 3, -1, 8, 0, -6, 1], the first negative element is the third in a row, and
# the sum of the modules of the array elements after it will be 8 + 0 + 6 + 1 = 15

