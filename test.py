class Stack:
    def __init__(self):
        self.items = []

    def is_empty(self):
        #return len(self.items) == 0
        return not self.items

    def push(self, num):
        self.items.append(num)

    def pop(self):
        return self.items.pop()

    def peek(self, *args):
        return self.items[-1]

    def size(self):
        return len(self.items)

    def __str__(self):
        return str(self.items)

if __name__ == "__main__":
    s = Stack()
    print(s)
    print(s.is_empty())
    s.push(3)
    print(s)
    s.push(1)
    s.push(2)
    print(s)
    s.peek(5)
    print(s)
    print(s.__dict__)